package com.springboot.first.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaGuidesSpringBootTest3Application {

	public static void main(String[] args) {
		SpringApplication.run(JavaGuidesSpringBootTest3Application.class, args);
	}

}
