package com.springboot.first.app;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {
	@GetMapping(path="/student", produces = MediaType.APPLICATION_JSON_VALUE)
	public Student getStudent() {
		return new Student("Dhanuka", "Madhusanka");
	}

	@GetMapping(path="/students", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Student> getStudents() {
		List<Student> students = new ArrayList<>();

		students.add(new Student("Dhanuka", "Madhusanka"));
		students.add(new Student("Dhanuka2", "Madhusanka2"));

		return students;
	}
}
